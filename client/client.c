#include <stdio.h>
#include <stdbool.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#define PORT 8080
#define sz 100
#define sz10b 1024
int sock = 0, valread, auth_ = 0;
char userlog[sz]={};

void read2d(char arr[][sz], int idx){
    for (int i=0; i<idx; i++){
        read(sock, arr[i], sizeof(arr[i]));
    }
}

void write2d(char arr[][sz], int idx){
    for (int i=0; i<idx; i++){
        write(sock, arr[i], sizeof(arr[i]));
    }
}

int string_to_int(char s[]){
    int len = strlen(s);
    int idx=0;
    for(int i = 0; i<len; i++){
        idx = idx * 10 + (s[i]-'0');
    }
    return idx;
}

void login(const char user[], const char pass[]){
    int status;
    char temp_buff[sz10b]={}, data_user[sz10b][sz]={}, pass_user[sz10b][sz]={};
    int idx;
    valread = read(sock, temp_buff, sz10b);
    idx = string_to_int(temp_buff);
    read2d(data_user, idx);

    for (int i=0; i<idx; i++){
        char *u, *p;
        u = strtok(data_user[i], ":");
        p = strtok(NULL, "\n");
        strcpy(data_user[i], u);
        strcpy(pass_user[i], p);
    }
    int auth_idx=-1;
    for (int i = 0; i < idx; i++){
        if (!strcmp(data_user[i], user)){
            auth_idx = i;
            break;
        }
    }
    
    if (auth_idx == -1){
        printf("Access denied!\n");
        return ;
    }

    if (!strcmp(pass, pass_user[auth_idx])){
        strcpy(userlog, data_user[auth_idx]);
        auth_ = 1;
        printf("Access granted!\n");
    }else {
        printf("Access denied!\n");
    }
}

void regist(char str[]){
    char feedback[sz10b] = {};
    write(sock, str, sz10b);
    valread = read(sock, feedback, sz10b);
    printf("%s\n", feedback);
}

void createDB(char str[]){
    char feedback[sz10b] = {};
    write(sock, str, sz10b);
    valread = read(sock, feedback, sz10b);
    printf("%s\n", feedback);
}

void createTable(char str[]){
    char feedback[sz10b] = {};
    write(sock, str, sz10b);
    valread = read(sock, feedback, sz10b);
    printf("%s\n", feedback);
}

void removeDB(char str[]){
    char feedback[sz10b] = {};
    write(sock, str, sz10b);
    valread = read(sock, feedback, sz10b);
    printf("%s\n", feedback);
}

void removeTable(char str[]){
    char feedback[sz10b] = {};
    write(sock, str, sz10b);
    valread = read(sock, feedback, sz10b);
    printf("%s\n", feedback);
}

void useDB(char str[]){
    char feedback[sz10b] = {};
    write(sock, str, sz10b);
    valread = read(sock, feedback, sz10b);
    printf("%s\n", feedback);
}

void userPermission(char str[]){
    char feedback[sz10b] = {};
    write(sock, str, sz10b);
    valread = read(sock, feedback, sz10b);
    printf("%s\n", feedback);
}

int command(){
    char choice[100];
    scanf("%[^\n]s", choice);
    getchar();
    write(sock, choice, sz10b);
    if (strstr(choice, "CREATE USER")){
        if (auth_ == 2) regist(choice);
        else printf("Permissions denied!\n");
    }else if (strstr(choice, "CREATE DATABASE")){
        createDB(choice);
    }else if (!strcmp(choice, "CLOSE DATABASE")){
        char feedback[sz10b] = {};
        valread = read(sock, feedback, sz10b);
        printf("%s\n", feedback);
    }else if (strstr(choice, "CREATE TABLE")){
        createTable(choice);
    }else if (strstr(choice, "DROP DATABASE")){
        removeDB(choice);
    }else if (strstr(choice, "DROP TABLE")){
        removeTable(choice);
    }else if (strstr(choice, "USE ")){
        useDB(choice);
    }else if (strstr(choice, "GRANT PERMISSION")){
        userPermission(choice);
    }
    
    else if (!strcmp(choice, "close")){
        return 0;
    }
    return 1;
}

void writeInt(int var, int size){
    char buffer[sz10b]={};
    sprintf(buffer, "%d", var);
    write(sock, buffer, sz10b);
}

int main(int argc, char const *argv[]) {
    struct sockaddr_in address;
    struct sockaddr_in serv_addr;
    char buffer[sz10b] = {0};
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        return -1;
    }
  
    memset(&serv_addr, '0', sizeof(serv_addr));
  
    serv_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
    
    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }
  
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("\nConnection Failed \n");
        return -1;
    }
    printf("Dont type anything, wait until another client disconnect!\n");
    if (argc == 5 && !strcmp(argv[1], "-u") && !strcmp(argv[3], "-p")){
        
        write(sock, "1", sz10b);
        login(argv[2], argv[4]);
    }else if (argc == 1){
        auth_ = 2;
        printf("Root access granted!\n");
        write(sock, "2", sz10b);
        strcpy(userlog, "root");
    }else {
        printf("Wrong Login Format !\n");
        return 0;
    }
    
    printf("Server free, you may type now\n");

    writeInt(auth_, sz10b);
    write(sock, userlog, sz10b);

    if(auth_){
        while(command());
    }
    close(sock);
    return 0;
}