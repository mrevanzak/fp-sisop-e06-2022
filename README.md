# SISOP Modul 5 (FP)

## Anggota Kelompok

| NRP        | NAMA                       |
| ---------- | -------------------------- |
| 5025201260 | Fadel Pramaputra Maulana   |
| 5025201079 | Julio Geraldi Soeiono      |
| 5025201145 | Mochamad Revanza Kurniawan |

## Daftar Isi

| Daftar                       | Status                       |
| ---------------------------- | ---------------------------- |
| Autentikasi                  | DONE                         |
| Authorisasi                  | DONE                         |
| Data Definition Language     | DONE                         |
| Data Manipulation Language   |                              |
| Logging                      |                              |
| Reliability                  |                              |
| Tambahan                     |                              |
| Error Handling               |                              |
| Extra                        |                              |
