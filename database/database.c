#include <stdio.h>
#include <ctype.h>
#include <stdbool.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <pthread.h> 
#include <dirent.h>
#define PORT 8080
#define sz10b 1024
#define sz 100

int valread;
char *users_file = "users.txt";
char userlog[sz10b]={}, currDatabase[sz10b]={};
int inDatabase = 0;

void write2d(char arr[][sz], int idx, int new_socket){
    for (int i=0; i<idx; i++){
        write(new_socket, arr[i], sizeof(arr[i]));
    }
}

int string_to_int(char s[]){
    int len = strlen(s);
    int idx=0;
    for(int i = 0; i<len; i++){
        idx = idx * 10 + (s[i]-'0');
    }
    return idx;
}

void toupper_string(char str[]){
    for(int i=0; i<strlen(str); i++){
        str[i] = toupper(str[i]);
    }
}

void writeInt(int var, int size, int new_socket){
    char buffer[sz]={};
    sprintf(buffer, "%d", var);
    write(new_socket, buffer, size);
}

int remove_directory(const char *path) {
    DIR *d = opendir(path);
    size_t path_len = strlen(path);
    int r = -1;

    if (d) {
        struct dirent *p;

        r = 0;
        while (!r && (p=readdir(d))) {
            int r2 = -1;
            char *buf;
            size_t len;

            /* Skip the names "." and ".." as we don't want to recurse on them. */
            if (!strcmp(p->d_name, ".") || !strcmp(p->d_name, ".."))
                continue;

            len = path_len + strlen(p->d_name) + 2; 
            buf = malloc(len);

            if (buf) {
                struct stat statbuf;

                snprintf(buf, len, "%s/%s", path, p->d_name);
                if (!stat(buf, &statbuf)) {
                    if (S_ISDIR(statbuf.st_mode))
                    r2 = remove_directory(buf);
                    else
                    r2 = unlink(buf);
                }
                free(buf);
            }
            r = r2;
        }
        closedir(d);
    }

    if (!r)
        r = rmdir(path);

    return r;
}
void login_handler(int new_socket){
    FILE* fp = fopen(users_file,"r");
	if(!fp) {
        printf("Unable to open %s\n", users_file);
        exit(0);
    }
    char line[sz10b], data_user[sz10b][sz]={};
    int idx=0;
    printf("Username and password list:\n");
    while (fgets(line, sz10b, fp)) {
        char *u;
        u = strtok(line, "\n");
        printf("-%s-\n", u); 
        strcpy(data_user[idx], u);
        idx++;
    }
    fclose(fp);
    char temp_buff[sz10b]={};
    sprintf(temp_buff, "%d", idx);
    write(new_socket, temp_buff, sz10b);
    write2d(data_user, idx, new_socket);
}

void regis_handler(int new_socket){
    char line[sz10b], data_user[sz10b][sz]={}, raw[sz10b] = {};
    int idx=0;
    FILE* fp = fopen(users_file,"r");
	if(!fp) {
        printf("Unable to open %s\n", users_file);
    }else {
        printf("Username List:\n"); 
        while (fgets(line, sizeof(line), fp)) {
            char *u;
            const char s[2] = ":";
            u = strtok(line, s);
            printf("-%s\n", u); 
            strcpy(data_user[idx], u);
            idx++;
        }
        fclose(fp);
    }
    char user[sz10b]={}, 
         pass[sz10b]={};

    valread = read(new_socket, raw, sz10b);
    char *token; int i = 1;
    token = strtok(raw, " ");
    while(token != NULL){
        if (i != 3 && i != 6){
            toupper_string(token);
        }

        if (i == 1 && strcmp(token, "CREATE")){
            write(new_socket, "Wrong create user syntax!", sz10b);
            return;
        }else if (i == 2 && strcmp(token, "USER")){
            write(new_socket, "Wrong create user syntax!", sz10b);
            return;
        }else if (i == 3){
            strcpy(user, token);
        }else if (i == 4 && strcmp(token, "IDENTIFIED")){
            write(new_socket, "Wrong create user syntax!", sz10b);
            return;
        }else if (i == 5 && strcmp(token, "BY")){
            write(new_socket, "Wrong create user syntax!", sz10b);
            return;
        }else if (i == 6){
            strcpy(pass, token);
        }

        if (i != 5)
            token = strtok(NULL, " ");
        else 
            token = strtok(NULL, ";");
        i++;
    }


    bool u_=false;
    for (i = 0; i < idx; i++){
        if (!strcmp(data_user[i], user)){
            u_=true;
            break;
        }
    }
    if (u_) {
        write(new_socket, "Username already exist!", sz10b);
    }else {
        fp = fopen(users_file,"a");
        fprintf(fp, "%s:%s\n", user, pass);
        printf("User [%s] Registered\n", user);
        write(new_socket, "User registered", sz10b);
        fclose(fp);
    }
}

void createDB(int new_socket){
    char str[sz10b];
    valread = read(new_socket, str, sz10b);
    char *token; 
    token = strtok(str, " "); toupper_string(token);
    if (strcmp(token, "CREATE")){
        write(new_socket, "Failed create database!", sz10b);
        return ;
    }
    token = strtok(NULL, " "); toupper_string(token);
    if (strcmp(token, "DATABASE")){
        write(new_socket, "Failed create database!", sz10b);
        return ;
    }
    token = strtok(NULL, ";");
    strcpy(currDatabase, token);
    mkdir(token ,0777);
    char path[sz10b] = {};
    sprintf(path, "%s/user.txt", token);
    FILE* fp = fopen(path,"a");
    fprintf(fp, "%s\n", userlog);
    printf("Database created by [%s]\n", userlog);
    write(new_socket, "Database created!", sz10b);
    fclose(fp);
}

void createTable(int new_socket){
    if (!inDatabase){
        write(new_socket, "You're not in any database!", sz10b);
        return;
    }
    char str[sz10b] = {}, strTemp[sz10b] = {};
    valread = read(new_socket, str, sz10b);
    strcpy(strTemp, str);
    char *namaTabel = strtok(strTemp, " ");
    for(int i=0; i<2; i++){
        namaTabel = strtok(NULL, " ");
    } 
    char file[sz10b*2]={};
    sprintf(file, "%s/%s.tsv", currDatabase, namaTabel);
    FILE* fp = fopen(file,"r");
	if(fp) {
        write(new_socket, "Tabel already exist!", sz10b);
        fclose(fp);
        return;
    }
    char *p = strstr(str, "(");
    char kolom[sz][sz10b] = {};
    char tipe[sz][sz10b] = {};
    char temp[sz10b] = {};
    int i, idx=0;
    for (int i=1, j=0; i<strlen(p)-1; i++){
        char c = p[i];
        if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c >= '0' && c <= '9')){
            char cStr[sz];
            sprintf(cStr, "%c", c);
            strcat(temp, cStr);
        }else if (strlen(temp)){
            if (j & 1){
                strcpy(tipe[idx], temp);
                idx++;
            }else {
                strcpy(kolom[idx], temp);
            }
            j++;
            strcpy(temp, "");
        }
    }
    fp = fopen(file,"a");
    for (int i=0; i<idx; i++){
        fprintf(fp, "%s:%s:\t", kolom[i], tipe[i]);
    }
    printf("Tabel created by [%s]\n", userlog);
    write(new_socket, "Tabel created!", sz10b);
    fclose(fp);
}

void removeDB(int new_socket){
    char str[sz10b];
    valread = read(new_socket, str, sz10b);
    char *token; 
    token = strtok(str, " "); toupper_string(token);
    if (strcmp(token, "DROP")){
        write(new_socket, "Failed drop database!", sz10b);
        return ;
    }
    token = strtok(NULL, " "); toupper_string(token);
    if (strcmp(token, "DATABASE")){
        write(new_socket, "Failed drop database!", sz10b);
        return ;
    }
    token = strtok(NULL, ";");
    DIR* dir = opendir(token);
    if(!dir) {
        write(new_socket, "Database is not exist!", sz10b);
        return;
    }else {
        closedir(dir);
    }

    if (!strcmp(userlog, "root")){
        remove_directory(token);
        printf("Database %s droped by [%s]\n", token, userlog);
        write(new_socket, "Database dropped!", sz10b);
        return;
    }

    char path[sz10b] = {}, line[sz10b] = {};
    sprintf(path, "%s/user.txt", token);
    FILE* fp = fopen(path,"r");
    while (fgets(line, sizeof(line), fp)) {
        char *tok = strtok(line, "\n");
        if (!strcmp(tok, userlog)){
            remove_directory(token);
            printf("Database %s droped by [%s]\n", token, userlog);
            write(new_socket, "Database dropped!", sz10b);
            fclose(fp);
            return;
        }
    }
    write(new_socket, "User not premitted!", sz10b);
    fclose(fp);
}

void removeTable(int new_socket){
    if (!inDatabase){
        write(new_socket, "You're not in any database!", sz10b);
        return ;
    }
    char str[sz10b];
    valread = read(new_socket, str, sz10b);
    char *token; 
    token = strtok(str, " "); toupper_string(token);
    if (strcmp(token, "DROP")){
        write(new_socket, "Failed drop database!", sz10b);
        return ;
    }
    token = strtok(NULL, " "); toupper_string(token);
    if (strcmp(token, "TABLE")){
        write(new_socket, "Failed drop table!", sz10b);
        return ;
    }
    token = strtok(NULL, ";");

    char path[sz10b*2] = {}, line[sz10b] = {};
    sprintf(path, "%s/%s.tsv", currDatabase, token);
    FILE* fp = fopen(path,"r");
    if (!fp){
        write(new_socket, "Table is not exist!", sz10b);
        return;
    }
    remove(path);
    printf("Table %s from database %s droped by [%s]\n", token, currDatabase, userlog);
    write(new_socket, "Table dropped!", sz10b);
    fclose(fp);
}

void useDB(int new_socket){
    char str[sz10b] = {};
    valread = read(new_socket, str, sz10b);
    char *token; 
    token = strtok(str, " "); toupper_string(token);
    if (strcmp(token, "USE")){
        write(new_socket, "Failed use database!", sz10b);
        return ;
    }
    token = strtok(NULL, ";");

    if (!strcmp(userlog, "root")){
        strcpy(currDatabase, token);
        inDatabase = 1;
        printf("User [%s] logged to database [%s]\n", userlog, currDatabase);
        write(new_socket, "User logged to database!", sz10b);
        return;
    }

    char path[sz10b] = {}, line[sz10b] = {};
    sprintf(path, "%s/user.txt", token);
    FILE* fp = fopen(path,"r");
    while (fgets(line, sizeof(line), fp)) {
        char *tok = strtok(line, "\n");
        if (!strcmp(tok, userlog)){
            strcpy(currDatabase, token);
            inDatabase = 1;
            printf("User [%s] logged to database [%s]\n", userlog, currDatabase);
            write(new_socket, "User logged to database!", sz10b);
            fclose(fp);
            return;
        }
    }
    write(new_socket, "User not permitted!", sz10b);
    fclose(fp);
}

void userPermission(int new_socket){
    if (strcmp(userlog, "root")){
        write(new_socket, "Failed grant user permission to database!", sz10b);
        return ;
    }
    char str[sz10b] = {}, database[sz10b] = {}, user[sz10b]={};
    valread = read(new_socket, str, sz10b);
    char *token; 
    token = strtok(str, " "); toupper_string(token);
    if (strcmp(token, "GRANT")){
        write(new_socket, "Failed grant user permission to database!", sz10b);
        return ;
    }
    token = strtok(NULL, " "); toupper_string(token);
    if (strcmp(token, "PERMISSION")){
        write(new_socket, "Failed grant user permission to database!", sz10b);
        return ;
    }token = strtok(NULL, " "); 
    strcpy(database, token);
    token = strtok(NULL, " "); toupper_string(token);
    if (strcmp(token, "INTO")){
        write(new_socket, "Failed grant user permission to database!", sz10b);
        return ;
    }
    token = strtok(NULL, ";");
    strcpy(user, token);
    DIR* dir = opendir(database);
    if(!dir) {
        write(new_socket, "Database is not exist!", sz10b);
        return;
    }else {
        closedir(dir);
    }
    char path[sz10b*2] = {};
    printf("User [%s] permission granted for database %s\n", user, database);
    write(new_socket, "Permission granted!", sz10b);
    sprintf(path, "%s/user.txt", database);
    FILE* fp = fopen(path,"a");
    fprintf(fp, "%s\n", user);
    fclose(fp);
}

int command(int auth_, int new_socket){
    char com[sz10b]={};
    valread = read(new_socket, com, sz10b);
    if (auth_ == 2 && strstr(com, "CREATE USER")){
        regis_handler(new_socket);
    }else if (strstr(com, "CREATE DATABASE")){
        createDB(new_socket);
        inDatabase = 1;
    }else if (!strcmp(com, "CLOSE DATABASE")){
        if (inDatabase){
            inDatabase = 0;
            printf("User [%s] log out from %s\n", userlog, currDatabase);
            strcpy(currDatabase, "");
            write(new_socket, "Database closed!", sz10b);
        }else write(new_socket, "You're not in any database!", sz10b);
    }else if (strstr(com, "CREATE TABLE")){
        createTable(new_socket);
    }else if (strstr(com, "DROP DATABASE")){
        removeDB(new_socket);
    }else if (strstr(com, "DROP TABLE")){
        removeTable(new_socket);
    }else if (strstr(com, "USE ")){
        useDB(new_socket);
    }else if (strstr(com, "GRANT PERMISSION")){
        userPermission(new_socket);
    }
    
    else if (!strcmp(com, "close")){
        return 0;
    }
    return 1;
}

void *client_connect_handler(void *sock){
    int new_socket = *(int*)sock;
    int auth_ = 0;
    char buffer[sz10b] = {}, authChoice[sz10b] = {};

    valread = read(new_socket, authChoice, sz10b);
    if (string_to_int(authChoice) == 1){
        login_handler(new_socket);
    }
    
    valread = read(new_socket, buffer, sz10b);
    auth_ = string_to_int(buffer);
    valread = read(new_socket, userlog, sizeof(userlog));
    
    if (auth_){
        printf("User [%s] logged in\n", userlog);
        while(command(auth_, new_socket));
    }
    free(sock);
}

int main(int argc, char const *argv[]) {
    int server_fd;
    struct sockaddr_in address;
    int opt = 1, new_socket;
    int addrlen = sizeof(address);
    
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }
      
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt failed");
        exit(EXIT_FAILURE);
    }
    
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = htonl(INADDR_ANY);
    address.sin_port = htons( PORT );
    
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))< 0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0) {
        perror("listen failed");
        exit(EXIT_FAILURE);
    }

    printf("Waiting for client connections...\n");
    while((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))){
        new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen);
        printf("Connection accepted\n");

        pthread_t sniffer_thread;
		int *new_sock = malloc(1);
		*new_sock = new_socket;
		
		if( pthread_create( &sniffer_thread , NULL ,  client_connect_handler , (void*) new_sock) < 0){
			perror("could not create thread");
			return 1;
		}
		
		pthread_join(sniffer_thread ,NULL);
		printf("\nDisconnected from client\n");
        printf("Waiting for client connections...\n");
    }
    
    if (new_socket < 0) {
        perror("accept failed");
        exit(EXIT_FAILURE);
    }

    return 0;
}
